@echo off
setlocal
setlocal enabledelayedexpansion
rem throw away everything except the IPv4 address line 
for /f "usebackq tokens=*" %%a in (`ipconfig ^| findstr 10.250 ^| findstr /i "ipv4"`) do (
  rem we have for example "IPv4 Address. . . . . . . . . . . : 192.168.42.78"
  rem split on : and get 2nd token
    for /f delims^=^:^ tokens^=2 %%b in ('echo %%a') do (
    rem we have " 192.168.42.78"
    rem split on . and get 4 tokens (octets)
      for /f "tokens=1-4 delims=." %%c in ("%%b") do (
      set _o1=%%c
      set _o2=%%d
      set _o3=%%e
      set _o4=%%f
      rem rem strip leading space from first octet
      set _4octet=!_o1:~1!.!_o2!.!_o3!.!_o4!
      echo !_4octet!
      )
    )
  )
rem add route using _4octet as Gateway
route ADD 10.3.0.0 MASK 255.255.0.0 !_4octet!
route ADD 10.250.0.0 MASK 255.255.0.0 !_4octet!
route ADD 10.251.0.0 MASK 255.255.0.0 !_4octet!
route ADD 10.252.0.0 MASK 255.255.0.0 !_4octet!
route ADD 172.30.0.0 MASK 255.255.0.0 !_4octet!
route ADD 172.31.0.0 MASK 255.255.0.0 !_4octet!
route ADD 202.162.18.0 MASK 255.255.255.0 !_4octet!
endlocal